﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LittleWarriorController : MonoBehaviour {

    public GameObject splashImage;
    public Text dialogueText;
    public GameObject arrowUI;

    public GameObject playerHealthBar;

    public bool canTap;

    [SerializeField]
    private Animator animator;

    public CharacterControllerLogic character;

	void Start () 
    {
        animator = GetComponent<Animator>();
        canTap = false;
	}
	
	void Update () 
    {
        if (canTap)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                animator.SetBool("Continue",true);
            }
        }

        if (IsWin())
        {
            splashImage.SetActive(true);
            dialogueText.text = "HEEEY!!";
        }

        if (IsTaunt())
        {
            dialogueText.text = "QUE SUERTE LA MIA DE QUE UN HOMBRE TAN FORTACHON PASE POR AQUI!";
            arrowUI.SetActive(true);
        }

        if (IsIdle2()|| IsIdle3() || IsIdle4())
        {
            canTap = true;
            if (Input.GetKeyDown(KeyCode.Return))
            {
                arrowUI.SetActive(false);
            }
        }

        if (IsPickUp())
        {
            animator.SetBool("Continue", false);
            canTap = false;
            dialogueText.text = "NECESITO SU AYUDA, MI ALDEA ESTA INVADIDA POR MONSTRUOS, TODOS HAN HUIDO PERO EN EL CUADRILATERO DE LUCHA...";
            StartCoroutine(ArrowSpawn(.8f));
        }

        if (IsLose())
        {
            animator.SetBool("Continue", false);
            canTap = false;
            dialogueText.text = "HA QUEDADO UN OBJETO MUY VALIOSO PARA NUESTRO PUEBLO, PODRIA AYUDARME A RECUPERARLO?";
            StartCoroutine(ArrowSpawn(.8f));
        }

        if (IsTaunt2())
        {
            animator.SetBool("Continue", false);
            canTap = false;
            dialogueText.text = "SI NECESITA AYUDA PRESIONE LA TECLA H, POR EL CAMINO ENCONTRARA COFRES, ASEGURESE DE AGARRARLOS PARA CONSEGUIR OBJETOS";
            StartCoroutine(ArrowSpawn(.8f));

            if (Input.GetKeyDown(KeyCode.Return))
            {
                arrowUI.SetActive(false);
                splashImage.SetActive(false);
                character.isInDialogue = false;
                playerHealthBar.SetActive(true);
            }

        }

	}

    public bool IsWin()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Win");
    }

    public bool IsTaunt()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Taunt");
    }

    public bool IsIdle2()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Idle 2");
    }

    public bool IsPickUp()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Pickup");
    }

    public bool IsIdle3()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Idle 3");
    }

    public bool IsLose()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Lose");
    }

    public bool IsIdle4()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Idle 4");
    }

    public bool IsTaunt2()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Taunt 2");
    }

    IEnumerator ArrowSpawn(float timeToSpawn)
    {
        yield return new WaitForSeconds(timeToSpawn);
        arrowUI.SetActive(true);
    }

}
