﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBox : MonoBehaviour 
{
    public CharacterControllerLogic target;
    public GameObject particle;
    public GameObject box;

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Axe" && target.IsAttacking())
        {
            particle.SetActive(true);
            box.SetActive(false);
            //StartCoroutine(DestroyParticle(3f));
        }
    }

    IEnumerator DestroyParticle(float timeToDestroy)
    {
        yield return new WaitForSeconds(timeToDestroy);
        particle.SetActive(false);
    }
}
