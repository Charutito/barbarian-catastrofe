﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchScript : MonoBehaviour
{

    public bool open;
    public bool close;
    public bool inTrigger;

    public GameObject splashImage;
    public Text dialogueText;

    public GameObject swit;
    public GameObject platform;

    void Start()
    {
        open = false;
        close = true;
    }

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        GUIDoor();

        if (inTrigger)
        {
            if (close)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    open = true;
                    close = false;
                    platform.transform.position = new Vector3(53.85f, 29.49f, -1.388428f);
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    close = true;
                    open = false;
                }
            }
        }

        if (open)
        {
            var newRot = Quaternion.Euler(new Vector3(-25f, 180f, 0f));
            //var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(00.0f, 90.0f, 0.0f), Time.deltaTime * 200);
            swit.transform.rotation = newRot;
        }
        else
        {
            var newRot = Quaternion.Euler(new Vector3(25f, 180f, 0f));
            //var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(00.00f, -45.0f, 0.0f), Time.deltaTime * 200);
            swit.transform.rotation = newRot;
        }
    }

    void GUIDoor()
    {

        if (inTrigger)
        {
            if (close)
            {
                splashImage.SetActive(true);
                dialogueText.text = "Presiona la tecla R para accionar el Switch";
            }
            else
            {
                splashImage.SetActive(false);
            }
        }
    }
}
