﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestRewards : MonoBehaviour {

    public GameObject weaponBox;
    public Image imageBox;
    public Sprite imageToShow;
    public Text _title;
    public Text _description;

    public string titleString;
    public string descriptionString;

    public Animator animator;

    private bool onSplashImage;
    private bool isOpen;

    public CharacterControllerLogic character;

    void Start()
    {
        animator = GetComponent<Animator>();   
     
	}
	
	void Update () 
    {
        if (onSplashImage)
        {
            if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Escape))
            {
                weaponBox.SetActive(false);
                character.isInDialogue = false;
                character.Animator.SetTrigger("PickObject");
                
                if (imageToShow.name == "Axe")
                    StartCoroutine(ShowAxe(.7f));
                else if(imageToShow.name == "Key")
                    DoorScript.doorKey = true;                
                
            }
        }
	}

    public void Open()
    {
        if (!isOpen)
        {
            animator.SetTrigger("Open");
            isOpen = true;
            StartCoroutine(SplashImage(1.0f));      
        }
        
    }

    IEnumerator SplashImage(float timeToSpawn)
    {
        yield return new WaitForSeconds(timeToSpawn);

        weaponBox.SetActive(true);
        onSplashImage = true;
        imageBox.sprite = imageToShow;
        _title.text = titleString;
        _description.text = descriptionString; 
    }

    IEnumerator ShowAxe(float timeToShow)
    {
        yield return new WaitForSeconds(timeToShow);
        character.Axe.SetActive(true);
    }
}
