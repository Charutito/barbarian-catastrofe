﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DoorScript : MonoBehaviour
{

    public static bool doorKey;
    public bool open;
    public bool close;
    public bool inTrigger;

    public GameObject splashImage;
    public Text dialogueText;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        GUIDoor();

        if (inTrigger)
        {
            if (close)
            {
                if (doorKey)
                {
                    if (Input.GetKeyDown(KeyCode.R))
                    {
                        open = true;
                        close = false;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    close = true;
                    open = false;
                }
            }
        }

        if (open)
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, 180.0f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
        else
        {
            var newRot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0.0f, 90f, 0.0f), Time.deltaTime * 200);
            transform.rotation = newRot;
        }
    }

    void GUIDoor()
    {

        if (inTrigger)
        {
            if (open)
            {
                splashImage.SetActive(true);
                dialogueText.text = "Presiona la tecla R para cerrar";
            }
            else
            {
                if (doorKey)
                {
                    splashImage.SetActive(true);
                    dialogueText.text = "Presiona la tecla E para abrir";
                }
                else
                {
                    splashImage.SetActive(true);
                    dialogueText.text = "¡Necesitas una llave!";
                }
            }
        }
        else
        {
            splashImage.SetActive(false);
        }
    }
    /*
    void OnGUI()
    {
        if (inTrigger)
        {
            splashImage.SetActive(true);
            dialogueText.text = "HEEEY!!";
            if (open)
            {
                GUI.Box(new Rect(0, 0, 200, 25), "Presiona la tecla E para cerrar");
            }
            else
            {
                if (doorKey)
                {
                    GUI.Box(new Rect(0, 0, 200, 25), "Presiona la tecla E para abrir");
                }
                else
                {
                    GUI.Box(new Rect(0, 0, 200, 25), "¡Necesitas una llave!");
                }
            }
        }
    }
     * */
}