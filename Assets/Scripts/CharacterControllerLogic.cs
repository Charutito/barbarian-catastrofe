using UnityEngine;
using System.Collections;

/// <summary>
/// #DESCRIPTION OF CLASS#
/// </summary>
public class CharacterControllerLogic : MonoBehaviour 
{
    public GameObject Axe;
    public GameObject equiped;
    public GameObject Unequiped;
    
    public bool isEquiped = false;
    public bool isBlocked = false;
    public bool isInDialogue = false;

    public Enemies enemy;
    public HealthManager health;

    public AudioSource walk;


	#region Variables (private)
	
	// Inspector serialized
	[SerializeField]
	private Animator animator;
	[SerializeField]
	private ThirdPersonCamera gamecam;
	[SerializeField]
	private float rotationDegreePerSecond = 120f;
	[SerializeField]
	private float directionSpeed = 1.5f;
	[SerializeField]
	private float directionDampTime = 0.25f;
	[SerializeField]
	private float speedDampTime = 0.05f;
	[SerializeField]
	private float fovDampTime = 3f;
	[SerializeField]
	private float jumpMultiplier = 1f;
	[SerializeField]
	private CapsuleCollider capCollider;
	[SerializeField]
	private float jumpDist = 1f;
	
	
	// Private global only
	private float leftX = 0f;
	private float leftY = 0f;
	private AnimatorStateInfo stateInfo;
	private AnimatorTransitionInfo transInfo;
	private float speed = 0f;
	private float direction = 0f;
	private float charAngle = 0f;
	private const float SPRINT_SPEED = 2.0f;	
	private const float SPRINT_FOV = 75.0f;
	private const float NORMAL_FOV = 60.0f;
	private float capsuleHeight;	
	
	
	// Hashes
    private int m_LocomotionId = 0;
	private int m_LocomotionPivotLId = 0;
	private int m_LocomotionPivotRId = 0;	
	private int m_LocomotionPivotLTransId = 0;	
	private int m_LocomotionPivotRTransId = 0;	
	
	#endregion
		
	
	#region Properties (public)

	public Animator Animator
	{
		get
		{
			return this.animator;
		}
	}

	public float Speed
	{
		get
		{
			return this.speed;
		}
	}
	
	public float LocomotionThreshold { get { return 0.2f; } }
	
	#endregion
	
	
	#region Unity event functions
	
	/// <summary>
	/// Use this for initialization.
	/// </summary>
	void Start() 
	{
		animator = GetComponent<Animator>();
		capCollider = GetComponent<CapsuleCollider>();
		capsuleHeight = capCollider.height;
        walk = GetComponent<AudioSource>();

		if(animator.layerCount >= 2)
		{
			animator.SetLayerWeight(1, 1);
		}		
		
		// Hash all animation names for performance
        m_LocomotionId = Animator.StringToHash("Base Layer.Locomotion");
		m_LocomotionPivotLId = Animator.StringToHash("Base Layer.LocomotionPivotL");
		m_LocomotionPivotRId = Animator.StringToHash("Base Layer.LocomotionPivotR");
		m_LocomotionPivotLTransId = Animator.StringToHash("Base Layer.Locomotion -> Base Layer.LocomotionPivotL");
		m_LocomotionPivotRTransId = Animator.StringToHash("Base Layer.Locomotion -> Base Layer.LocomotionPivotR");

        isInDialogue = true;
        walk.Play();
        walk.Pause();
	}
	
	/// <summary>
	/// Update is called once per frame.
	/// </summary>
	void Update() 
	{
        if (!isInDialogue)
        {
            if (animator) //&& gamecam.CamState != ThirdPersonCamera.CamStates.FirstPerson)
            {
                stateInfo = animator.GetCurrentAnimatorStateInfo(0);
                transInfo = animator.GetAnimatorTransitionInfo(0);

                // Press A to jump
                if (Input.GetButton("Jump") && !isBlocked)
                {
                    animator.SetBool("Jump", true);
                }
                else
                {
                    animator.SetBool("Jump", false);
                }

                if (Input.GetButtonDown("Fire1") && !isBlocked)
                {
                    animator.SetTrigger("Attack1Trigger");
                    isBlocked = true;
                    if (isEquiped)
                    {
                        StartCoroutine(Attack(1.09f));
                    }
                    else
                    {
                        StartCoroutine(Attack(1.26f));
                    }
                }

                // Pull values from controller/keyboard
                leftX = Input.GetAxis("Horizontal");
                leftY = Input.GetAxis("Vertical");

                charAngle = 0f;
                direction = 0f;
                float charSpeed = 0f;

                // Translate controls stick coordinates into world/cam/character space
                StickToWorldspace(this.transform, gamecam.transform, ref direction, ref charSpeed, ref charAngle, IsInPivot());

                // Press B to sprint
                if (Input.GetButton("Sprint") && !isBlocked)
                {
                    speed = Mathf.Lerp(speed, SPRINT_SPEED, Time.deltaTime);
                    gamecam.GetComponent<Camera>().fieldOfView = Mathf.Lerp(gamecam.GetComponent<Camera>().fieldOfView, SPRINT_FOV, fovDampTime * Time.deltaTime);
                }
                else
                {
                    speed = charSpeed;
                    gamecam.GetComponent<Camera>().fieldOfView = Mathf.Lerp(gamecam.GetComponent<Camera>().fieldOfView, NORMAL_FOV, fovDampTime * Time.deltaTime);
                }

                animator.SetFloat("Speed", speed, speedDampTime, Time.deltaTime);
                animator.SetFloat("Direction", direction, directionDampTime, Time.deltaTime);


                if (speed <= 0)
                    walk.Play();


                if (speed > LocomotionThreshold)	// Dead zone
                {
                    if (!IsInPivot())
                    {
                        Animator.SetFloat("Angle", charAngle);
                    }
                }
                if (speed < LocomotionThreshold && Mathf.Abs(leftX) < 0.05f)    // Dead zone
                {
                    animator.SetFloat("Direction", 0f);
                    animator.SetFloat("Angle", 0f);
                }

                if (Input.GetKeyDown(KeyCode.F) && speed < 0.3f)
                {
                    if (isEquiped)
                    {
                        animator.SetBool("toEquip", false);
                        isBlocked = true;
                        StartCoroutine(UnEquip(1.0f));
                    }
                    else
                    {
                        animator.SetBool("toEquip", true);
                        isBlocked = true;
                        StartCoroutine(Equip(1.0f));
                    }
                }
            }
        }
		
	}
	
	/// <summary>
	/// Any code that moves the character needs to be checked against physics
	/// </summary>
	void FixedUpdate()
	{

        if (!isBlocked)
        {
            if (IsInLocomotion() && gamecam.CamState != ThirdPersonCamera.CamStates.Free && !IsInPivot() && ((direction >= 0 && leftX >= 0) || (direction < 0 && leftX < 0)))
            {
                Vector3 rotationAmount = Vector3.Lerp(Vector3.zero, new Vector3(0f, rotationDegreePerSecond * (leftX < 0f ? -1f : 1f), 0f), Mathf.Abs(leftX));
                Quaternion deltaRotation = Quaternion.Euler(rotationAmount * Time.deltaTime);
                this.transform.rotation = (this.transform.rotation * deltaRotation);
            }

            if (IsInJump())
            {
                float oldY = transform.position.y;
                transform.Translate(Vector3.up * jumpMultiplier * animator.GetFloat("JumpCurve"));
                if (IsInLocomotionJump())
                {
                    transform.Translate(Vector3.forward * Time.deltaTime * jumpDist);
                }
                capCollider.height = capsuleHeight + (animator.GetFloat("CapsuleCurve") * 0.5f);
                if (gamecam.CamState != ThirdPersonCamera.CamStates.Free)
                {
                    gamecam.transform.Translate(Vector3.up * (transform.position.y - oldY));
                }
            }
        }
		// Rotate character model if stick is tilted right or left, but only if character is moving in that direction
		
	}
	
	/// <summary>
	/// Debugging information should be put here.
	/// </summary>
	void OnDrawGizmos()
	{	
	
	}

    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Chest" && Input.GetKeyDown(KeyCode.R))
        {
            isInDialogue = true;
            ChestRewards chest = col.gameObject.GetComponent<ChestRewards>();
            chest.Open();
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Sword")
        {
            health.HurtPlayer(10);
        }
    }
	
	#endregion
	
	
	#region Methods
	
	public bool IsInJump()
	{
        return (IsInIdleJump() || IsInLocomotionJump() || IsInEquipIdleJump() || IsInEquipLocomotionJump());
	}

	public bool IsInIdleJump()
	{
		return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.IdleJump");
	}

    public bool IsInLocomotion()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Locomotion");
    }
	
	public bool IsInLocomotionJump()
	{
		return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.LocomotionJump");
	}

    public bool IsInEquipIdleJump()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Equip IdleJump");
    }

    public bool IsInEquipLocomotionJump()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Equip LocomotionJump");
    }
	
	public bool IsInPivot()
	{
		return stateInfo.nameHash == m_LocomotionPivotLId || 
			stateInfo.nameHash == m_LocomotionPivotRId || 
			transInfo.nameHash == m_LocomotionPivotLTransId || 
			transInfo.nameHash == m_LocomotionPivotRTransId;
	}

    public bool IsAttacking()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Equip Attack");
    }

    /*
    public bool IsInLocomotion()
    {
        return stateInfo.nameHash == m_LocomotionId;
    }
	*/
      
	public void StickToWorldspace(Transform root, Transform camera, ref float directionOut, ref float speedOut, ref float angleOut, bool isPivoting)
    {
        Vector3 rootDirection = root.forward;
				
        Vector3 stickDirection = new Vector3(leftX, 0, leftY);
		
		speedOut = stickDirection.sqrMagnitude;		

        // Get camera rotation
        Vector3 CameraDirection = camera.forward;
        CameraDirection.y = 0.0f; // kill Y
        Quaternion referentialShift = Quaternion.FromToRotation(Vector3.forward, Vector3.Normalize(CameraDirection));

        // Convert joystick input in Worldspace coordinates
        Vector3 moveDirection = referentialShift * stickDirection;
		Vector3 axisSign = Vector3.Cross(moveDirection, rootDirection);
		
		Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2f, root.position.z), moveDirection, Color.green);
		Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2f, root.position.z), rootDirection, Color.magenta);
		Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2f, root.position.z), stickDirection, Color.blue);
		Debug.DrawRay(new Vector3(root.position.x, root.position.y + 2.5f, root.position.z), axisSign, Color.red);
		
		float angleRootToMove = Vector3.Angle(rootDirection, moveDirection) * (axisSign.y >= 0 ? -1f : 1f);
		if (!isPivoting)
		{
			angleOut = angleRootToMove;
		}
		angleRootToMove /= 180f;
		
		directionOut = angleRootToMove * directionSpeed;
	}

    IEnumerator Equip(float timeToEquip)
    {
        yield return new WaitForSeconds(timeToEquip);
        Axe.transform.parent = equiped.transform;
        isEquiped = true;
        Axe.transform.localPosition = Vector3.zero;
        Axe.transform.localRotation = Quaternion.identity;
        isBlocked = false;
    }

    IEnumerator UnEquip(float timeToEquip)
    {
        yield return new WaitForSeconds(timeToEquip);
        Axe.transform.parent = Unequiped.transform;
        isEquiped = false;
        Axe.transform.localPosition = Vector3.zero;
        Axe.transform.localRotation = Quaternion.identity;
        isBlocked = false;
    }

    IEnumerator Attack(float timeToWalk)
    {
        yield return new WaitForSeconds(timeToWalk);
        isBlocked = false;
    }

	#endregion Methods
}
