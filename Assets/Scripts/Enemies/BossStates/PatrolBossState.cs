﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolBossState : BossState 
{

    public PatrolBossState(StateMachine sm, Boss r)
        : base(sm, r)
    {
    }

    public override void Awake()
    {
        base.Awake();
    }

    public override void Execute()
    {
        base.Execute();
        boss.Patrol();
    }

    public override void Sleep()
    {
        base.Sleep();
    }
}
