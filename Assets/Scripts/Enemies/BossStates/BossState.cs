﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossState : State
{
    protected Boss boss;

    public BossState(StateMachine sm, Boss r)
        : base(sm)
    {
        boss = r;
    }
}
