﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBossState : BossState 
{

    public DeathBossState(StateMachine sm, Boss r)
        : base(sm, r)
    {

    }

    public override void Awake()
    {
        base.Awake();
    }

    public override void Execute()
    {
        base.Execute();
        boss.Death();
    }

    public override void Sleep()
    {
        base.Sleep();
    }
}
