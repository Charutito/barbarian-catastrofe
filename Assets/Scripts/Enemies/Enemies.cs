﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class Enemies : MonoBehaviour
{


    public float speed;
    public float rotationSpeed;

    private StateMachine _sm;

    public CharacterControllerLogic target;

    // Referencias para la linea de vision

    public float viewAngle;
    public float viewDistance;
    private Vector3 _dirToTarget;
    private float _angleToTarget;
    private float _distanceToTarget;
    private bool _targetInSight;

    //Animator
    private Animator anim;

    //Waypoints
    public int currentWaypoint;
    public Waypoint[] waypoints;
    public float turn = 1.5f;
    public bool doneWay;

    //Attack
    public float timeOfPrediction;
    private Vector3 _predictedPosition = Vector3.zero;

    public float TimetoChange;

    //Stats
    public int health;
    public int maxHealth = 10;
    public Image healthBar;

    public Explosion particleDeath;
    public bool isDeath;
    public GameObject objects;

    public bool isAttacking;

    void Start()
    {
        _sm = new StateMachine();
        _sm.AddState(new IdleSkeletonState(_sm, this));
        _sm.AddState(new PatrolSkeletonState(_sm, this));
        _sm.AddState(new AttackSkeletonState(_sm, this));
        _sm.AddState(new DeathSkeletonState(_sm, this));

        anim = GetComponent<Animator>();
        health = maxHealth;
        
    }

    public void Idle()
    {
        anim.SetBool("isWalking", false);

        if (TimetoChange > 0)
        {
            TimetoChange -= 1f * Time.deltaTime;
        }
        else
        {
            _sm.SetState<PatrolSkeletonState>();
            TimetoChange = 5;
        }
    }

    void Update()
    {
        if(!isDeath)
            _sm.Update();

        LineOfSight();        

        if (_targetInSight)
            _sm.SetState<AttackSkeletonState>();

        if(health == 0)
            _sm.SetState<DeathSkeletonState>();
    }

    public void GetHit(int damage)
    {
        health -= damage;
        anim.SetTrigger("getHit");
        healthBar.fillAmount = (float)health / (float)maxHealth;
    }

    public void Death()
    {
        anim.SetBool("death", true);
        isDeath = true;
        StartCoroutine(Explotion(2f));
        StartCoroutine(DestroyEnemy(8f));
    }

    public void Attack()
    {
        _distanceToTarget = Vector3.Distance(transform.position, target.transform.position);

        if (_distanceToTarget < 2.0f)
        {
            anim.SetTrigger("Attack");
            isAttacking = true;
        }
        else
        {
            anim.SetBool("isWalking", true);
            _predictedPosition = target.transform.position + target.transform.forward * target.Speed * timeOfPrediction;
            transform.forward = Vector3.Lerp(transform.forward, _predictedPosition - transform.position, rotationSpeed * Time.deltaTime);
            transform.position += transform.forward * speed * Time.deltaTime;
        }

        if (!_targetInSight)
        {
            _sm.SetState<IdleSkeletonState>();
            isAttacking = false;
        }           
    }


    public void Patrol()
    {
        var dirToWaypoint = waypoints[currentWaypoint].transform.position - transform.position;
        dirToWaypoint.y = transform.forward.y;
        transform.forward = Vector3.Slerp(transform.forward, dirToWaypoint, rotationSpeed * Time.deltaTime);
        transform.position += transform.forward * speed * Time.deltaTime;
        anim.SetBool("isWalking", true);
        doneWay = false;

        if (Vector3.Distance(transform.position, waypoints[currentWaypoint].transform.position) <= turn)
        {
            if (!doneWay)
                currentWaypoint++;

            if (currentWaypoint > waypoints.Length - 1)
            {
                doneWay = true;
                _sm.SetState<IdleSkeletonState>();
                currentWaypoint = 0;
                Array.Reverse(waypoints);
            }
        }
    }

    void LineOfSight()
    {
        _dirToTarget = target.transform.position - transform.position; //Siempre la dirección desde un punto a otro es: Posición Final - Posición Inicial

        _angleToTarget = Vector3.Angle(transform.forward, _dirToTarget); //Vector3.Angle nos da el ángulo entre dos direcciones

        _distanceToTarget = Vector3.Distance(transform.position, target.transform.position); //Vector3.Distance nos da la distancia entre dos posiciones


        if (_angleToTarget <= viewAngle && _distanceToTarget <= viewDistance)
        {
            /*
            Una vez que descartamos las primeras posibilidades, vamos a utilizar un raycast.
            */
            RaycastHit[] rch;
            _targetInSight = true;
            rch = Physics.RaycastAll(transform.position, _dirToTarget, _distanceToTarget);

            for (int i = 0; i < rch.Length; i++)
            {
                RaycastHit hit = rch[i];
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Level"))
                    _targetInSight = false;
            }
        }
        else //Si no se cumplieron las condiciones
            _targetInSight = false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Axe" && target.IsAttacking())
        {
            GetHit(5);
        }
    }


    void OnDrawGizmos()
    {

        if (_targetInSight)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, target.transform.position);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, viewDistance);

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + (transform.forward * viewDistance));

        Vector3 rightLimit = Quaternion.AngleAxis(viewAngle, transform.up) * transform.forward;
        Gizmos.DrawLine(transform.position, transform.position + (rightLimit * viewDistance));

        Vector3 leftLimit = Quaternion.AngleAxis(-viewAngle, transform.up) * transform.forward;
        Gizmos.DrawLine(transform.position, transform.position + (leftLimit * viewDistance));
    }

    IEnumerator Explotion(float timeToDeath)
    {
        yield return new WaitForSeconds(timeToDeath);
        particleDeath.Play();
        objects.SetActive(false);
    }

    IEnumerator DestroyEnemy(float timeToDestroy)
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(this.gameObject);
    }
}
