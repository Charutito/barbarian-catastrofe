﻿using UnityEngine;
using System.Collections;

public class IdleSkeletonState : SkeletonState
{

    public IdleSkeletonState(StateMachine sm, Enemies r)
        : base(sm, r)
    {
    }

    public override void Awake()
    {
        Debug.Log("Entró a Idle");
        base.Awake();
    }

    public override void Execute()
    {
        base.Execute();
        skeleton.Idle();
    }

    public override void Sleep()
    {
        Debug.Log("Salió de Idle");
        base.Sleep();
    }
}
