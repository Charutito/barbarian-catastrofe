﻿using UnityEngine;
using System.Collections;

public class AttackSkeletonState : SkeletonState 
{

    public AttackSkeletonState(StateMachine sm, Enemies r)
        : base(sm, r)
    {

    }

    public override void Awake()
    {
        Debug.Log("Entró a Attack");
        base.Awake();
    }

    public override void Execute()
    {
        base.Execute();
        skeleton.Attack();
    }

    public override void Sleep()
    {
        Debug.Log("Salió de Attack");
        base.Sleep();
    }
}
