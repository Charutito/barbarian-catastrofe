﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSkeletonState : SkeletonState 
{
    public DeathSkeletonState(StateMachine sm, Enemies r)
        : base(sm, r)
    {

    }

    public override void Awake()
    {
        base.Awake();
    }

    public override void Execute()
    {
        base.Execute();
        skeleton.Death();
    }

    public override void Sleep()
    {
        base.Sleep();
    }
}
