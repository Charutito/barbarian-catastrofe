﻿using UnityEngine;
using System.Collections;

public class PatrolSkeletonState : SkeletonState {

    public PatrolSkeletonState(StateMachine sm, Enemies r)
        : base(sm, r)
    {
    }
    
    public override void Awake()
    {
        Debug.Log("Entró a Patrol");
        base.Awake();
    }

    public override void Execute()
    {
        base.Execute();
        skeleton.Patrol();
    }

    public override void Sleep()
    {
        Debug.Log("Salió de Patrol");
        base.Sleep();
    }
}
