﻿using UnityEngine;
using System.Collections;

public class SkeletonState : State
{
    protected Enemies skeleton;

    public SkeletonState(StateMachine sm, Enemies r)
        : base(sm)
    {
        skeleton = r;
    }
}
