﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public int playerHealth;
    public int maxPlayerHealth = 100;

    public Image healthBar;

    void Awake()
    {
        playerHealth = maxPlayerHealth;
        SetHealthBar();
    }

    void Update()
    {
        SetHealthBar();
    }

    void SetHealthBar()
    {
        healthBar.fillAmount = (float)playerHealth / (float)maxPlayerHealth;
    }

    public void HurtPlayer(int damaged)
    {
        playerHealth -= damaged;
    }
}
